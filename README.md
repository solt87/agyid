agyid
=====

Introduction
------------

agyid is a simple command-line flashcard program. The name comes from the
Hungarian word "agyidegek", which means "cranial nerves".

Installation
------------

agyid itself needs no installation, but it needs a working [Python 3][py3] system.
Perform whatever magic is needed to set up Python 3 on your machine.

[py3]: https://www.python.org/downloads/
