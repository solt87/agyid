#! /usr/bin/env python3

# Copyright 2013-2018 Solt Budavari

# agyid is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# agyid is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with agyid.  If not, see <http://www.gnu.org/licenses/>.




import random, sys



## ================
## Data definitions:


## Question and Answer are String
## Interp. A question, and an answer to the question. Roles may or may not be reversed.
Q1 = "John Doe"
A1 = "inventor of foobar"
Q2 = "Mirror of Erised"
A2 = "shows your heart's desire"

#def fn_for_q_or_a(qa):  # Template
#        ... qa
#        return ...



## Card is Tuple(Question, Answer)
## Interp. A flash-card: a question and its corresponding answer.
C1 = Q1, A1
C2 = Q2, A2

#def fn_for_card(card):  # Template
#        ... card[0]
#        ... card[1]
#        return ...




## Set-of-Cards is (listof Card)
## Interp. A set of flash-cards belonging together.
S1 = [C1, C2]

#def fn_for_soc(soc):  # Template
#        for card in soc:
#                fn_for_card(card)
#        return ...




## ================
## Functions:


def main():
        filename = get_filename()
        soc = read_set(filename)
        test_set(shuffle_cards(soc))
        soc = shuffle_cards(soc)
        test_set_reverse(soc)
        
        return 0


def get_filename():
        """
        None -> String
        Get the name of the file that contains the "card" data.
        """
        
        result = ""
        if len(sys.argv) == 2:
                result = sys.argv[1]
        else:
                result = input("Please enter the name of the file to open: ")
        
        return result

def read_set(filename):
        """
        String -> Set-of-Cards
        Read a set of cards from the file given in 'filename'.
        """
        cards = []
        txt = open(filename, "r")
        for line in txt:
                card = read_card(line)
                cards.append(card)
        txt.close()
        return cards



def read_card(txt):
        """
        String -> Card
        Reads a card from a given string formatted as "question:answer".
        
        >>> read_card(Q1 + ":" + A1)
        ("John Doe", "inventor of foobar")
        """
        sep = txt.find(":")
        return txt[:sep].strip(), txt[sep+1 : -1].strip()



def shuffle_cards(soc):
        """
        Set-of-Cards -> Set-of-Cards
        Return a set of cards with same cards as set_of_cards, but in random order.
        """
        tmp = list(soc)
        new_set = []
        while len(tmp) > 0:
                rnum = random.randint(0, (len(tmp)-1))
                new_set.append(tmp.pop(rnum))
        return new_set



def test_set(soc):
        """
        (listof Cards) -> None
        Test the user with each card in listofcards.
        """
        for card in soc:
                test_card(card, 0)
        return



def test_card(card, q):
        """
        Card Integer -> None
        Print card[q], ask for answer; if user's answer is incorrect,
        print the correct answer, otherwise don't do anything.
        """
        a = 1 if q == 0 else 0
        useranswer = input(card[q] + ": ").strip().lower()
        if useranswer != card[a].lower():
                print(card[a])
        return



def test_set_reverse(soc):
        """
        (listof Cards) -> None
        Test the user with each card, but reverse the question-answer pairs.
        """
        for card in soc:
                test_card(card, 1)
        return




if __name__ == "__main__":
        main()
